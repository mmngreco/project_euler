"""

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""
import numpy as np


def prime_below(x):
    """Sums primes number find it below `x`.

    Parameters
    ----------
    x : int

    Returns
    -------
    sum_of_primes : int
    """
    prime_list = []
    ops = np.arange(2, x + 1)
    while len(ops):
        prime = ops[0]
        idx = np.nonzero(ops % prime != 0)
        ops = ops[idx]
        prime_list.append(prime)
    return prime_list

if __name__ == '__main__':
    x = 600851475143
    prime_list = prime_below(x)
    candidates = np.arange(2, x)
    is_factor = (x % candidates) == 0
    candidates = candidates[is_factor]
    prime_factors = list(filter(lambda x: x in prime_list, candidates))
    print(ou)


