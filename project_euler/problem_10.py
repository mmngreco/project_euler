
import numpy as np

"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.

"""


def sum_prime_below(x):
    """Sums primes number find it below `x`.

    Parameters
    ----------
    x : int

    Returns
    -------
    sum_of_primes : int
    """
    prime_list = []
    ops = np.arange(2, x + 1)
    while len(ops):
        prime = ops[0]
        idx = np.nonzero(ops % prime != 0)
        ops = ops[idx]
        prime_list.append(prime)
    return sum(prime_list)
